package com.example.springmvc.controller;

import com.example.springmvc.model.User;
import com.example.springmvc.model.enums.UserType;
import com.example.springmvc.security.SpringUser;
import com.example.springmvc.service.CategoryService;
import com.example.springmvc.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@AllArgsConstructor
public class MainController {


    private final PasswordEncoder passwordEncoder;

    private final UserService userService;

    private final CategoryService categoryService;



    @GetMapping("/")
    public String main() {

        return "index";

    }

    @GetMapping("/login")
    public String loginPage() {

        return "index";

    }

    @GetMapping("/loginSuccess")
    public String loginSuccess(@AuthenticationPrincipal SpringUser springUser) {

        if (springUser.getUser().getUserType() == UserType.ADMIN) {

            return "redirect:/admins/home";

        }else {
            return "redirect:/users/home";
        }



    }

    @GetMapping("/registration")
    public String registerPage() {

        return "registration";

    }


    @PostMapping("/registration")
    public String register(@ModelAttribute User user) {


        if (userService.getByEmail(user.getEmail()) != null) {
            return "registration";
        } else {

            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userService.save(user);
            return "index";

        }

    }



}
