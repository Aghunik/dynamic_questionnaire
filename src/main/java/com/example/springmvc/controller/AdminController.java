package com.example.springmvc.controller;

import com.example.springmvc.model.enums.UserType;
import com.example.springmvc.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/admins")
public class AdminController {


    private final UserService userService;

    @GetMapping("/home")
    public String home(ModelMap modelMap) {
        modelMap.addAttribute("users", userService.getByUserType(UserType.USER));
        return "adminHome";
    }

    @GetMapping("/delete")
    public String deleteUser(@RequestParam("id") int id) {

        if (userService.getById(id) != null) {
            userService.delete(id);
        }
        return "redirect:/admins/home";
    }


}
