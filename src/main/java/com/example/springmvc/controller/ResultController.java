package com.example.springmvc.controller;

import com.example.springmvc.model.Answer;
import com.example.springmvc.model.Question;
import com.example.springmvc.model.Result;
import com.example.springmvc.model.Test;
import com.example.springmvc.security.SpringUser;
import com.example.springmvc.service.*;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.LinkedList;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/results")
public class ResultController {


    private final UserService userService;

    private final CategoryService categoryService;

    private final TestService testService;

    private final QuestionService questionService;

    private final AnswerService answerService;

    private final ResultService resultService;

    @GetMapping("/all")
    public String allResult( @AuthenticationPrincipal SpringUser springUser,
                             ModelMap modelMap){
        List<Result> byUserId = resultService.getByUserId(springUser.getUser().getId());
        modelMap.addAttribute("results", byUserId);
        return "userResult";
    }


    @PostMapping("/save")
    public String result(@RequestParam("answers[]") List<Integer> answers,
                         @RequestParam("testId") int testId,
                         @AuthenticationPrincipal SpringUser springUser,
                         ModelMap modelMap) {
        int count = 0;
        Result result = new Result();
        result.setUser(springUser.getUser());
        for (Integer id : answers) {
            if (answerService.getById(id).isRight()) {
                count++;
            }
        }
        result.setTest(testService.getById(testId));
        result.setResult(count);
        resultService.save(result);
        modelMap.addAttribute("results", resultService.getByUserId(springUser.getUser().getId()));
        return "redirect:/results/all";
    }



}
