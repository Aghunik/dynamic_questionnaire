package com.example.springmvc.controller;

import com.example.springmvc.model.User;
import com.example.springmvc.security.SpringUser;
import com.example.springmvc.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/users")
public class UserController {


    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    @GetMapping("/home")
    public String home(@AuthenticationPrincipal SpringUser springUser,
                       ModelMap modelMap) {
        modelMap.addAttribute("user", springUser.getUser());

        return "userPages";
    }

    @GetMapping("/myAccount")
    public String account(@AuthenticationPrincipal SpringUser springUser,
                          ModelMap modelMap) {
        modelMap.addAttribute("user", springUser.getUser());

        return "userMyAccount";
    }

    @GetMapping("/update")
    public String update() {
        return "userUpdate";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute User user,
                         @AuthenticationPrincipal SpringUser springUser,
                         ModelMap modelMap) {
        User byId = userService.getById(springUser.getUser().getId());
        byId.setName(user.getName());
        byId.setSurname(user.getSurname());
        byId.setEmail(user.getEmail());
        byId.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.save(byId);
        modelMap.addAttribute("user", byId);
        return "userMyAccount";

    }


}
