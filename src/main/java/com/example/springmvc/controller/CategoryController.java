package com.example.springmvc.controller;

import com.example.springmvc.model.Category;
import com.example.springmvc.service.CategoryService;
import com.example.springmvc.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/categories")
public class CategoryController {


    private final UserService userService;

    private final CategoryService categoryService;


    @GetMapping("/all")
    public String categories(ModelMap modelMap) {
        modelMap.addAttribute("categories", categoryService.get());
        return "adminAllCategories";
    }

    @GetMapping("/save")
    public String categoryAdd() {

        return "adminAddCategory";

    }

    @PostMapping("/save")
    public String save(@ModelAttribute Category category) {

        if (categoryService.getByName(category.getName()) == null) {
            categoryService.save(category);
        }

        return "redirect:/categories";
    }

    @GetMapping("/delete")
    public String deleteCategory(@RequestParam("id") int id) {

        if (userService.getById(id) != null) {
            userService.delete(id);
        }
        return "redirect:/admins/home";
    }


}
