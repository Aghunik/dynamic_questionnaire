package com.example.springmvc.controller;

import com.example.springmvc.model.Answer;
import com.example.springmvc.model.Question;
import com.example.springmvc.model.Result;
import com.example.springmvc.model.Test;
import com.example.springmvc.security.SpringUser;
import com.example.springmvc.service.*;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.LinkedList;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/tests")
public class TestController {


    private final UserService userService;

    private final CategoryService categoryService;

    private final TestService testService;

    private final QuestionService questionService;

    private final AnswerService answerService;




    @GetMapping("/all")
    public String tests(ModelMap modelMap) {
        modelMap.addAttribute("tests", testService.get());
        return "adminAllTests";
    }

    @GetMapping("/save")
    public String testSave(ModelMap modelMap) {
        modelMap.addAttribute("questions", questionService.get());
        return "adminAddTest";

    }

    @PostMapping("/save")
    public String save(@ModelAttribute Test test) {

        testService.save(test);

        return "redirect:/tests/all";
    }

    @GetMapping("/delete")
    public String deleteTest(@RequestParam("id") int id) {


        if (testService.getById(id) != null) {
            userService.delete(id);
        }
        return "redirect:/admins/home";
    }
    @GetMapping("/deleteQuestion")
    public String deleteQuestions(@RequestParam("id") int id, @RequestParam("testId") int testId, RedirectAttributes redirectAttributes) {

      testService.deletQuestionById(testId,id);
      redirectAttributes.addAttribute("id", testId);
        return "redirect:/questions/all";
    }


    @GetMapping("/allTests")
    public String allTests(ModelMap modelMap) {
        modelMap.addAttribute("tests", testService.get());
        return "userAllTests";
    }
    @GetMapping("/view")
    public String view(@RequestParam("id") int id, ModelMap modelMap) {
        Test test = testService.getById(id);
        List<Answer> answers = new LinkedList<>();

        for (Question question : test.getQuestions()) {
            List<Answer> byQuestionId = answerService.getByQuestionId(question.getId());
            for (Answer answer : byQuestionId) {
                answers.add(answer);
            }

        }
        modelMap.addAttribute("test", test);
        modelMap.addAttribute("answers", answers);
        return "userTest";
    }



}
