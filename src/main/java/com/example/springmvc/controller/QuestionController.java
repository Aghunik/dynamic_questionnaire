package com.example.springmvc.controller;

import com.example.springmvc.model.Answer;
import com.example.springmvc.model.Question;
import com.example.springmvc.model.Test;
import com.example.springmvc.service.AnswerService;
import com.example.springmvc.service.CategoryService;
import com.example.springmvc.service.QuestionService;
import com.example.springmvc.service.TestService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/questions")
public class QuestionController {

    private final CategoryService categoryService;

    private final QuestionService questionService;

    private final AnswerService answerService;

    private final TestService testService;




    @GetMapping("/save")
    public String questions(ModelMap modelMap) {

        modelMap.addAttribute("categories", categoryService.get());

        return "adminAddQuestion";
    }


    @PostMapping("/save")
    public String save(@ModelAttribute("question") Question question,
                       @ModelAttribute("answer") Answer a, @RequestParam("status") String status) {
        int num = Integer.parseInt(status);
        int count = 0;


        questionService.save(question);
        String[] split = a.getAnswerText().split(",");

        for (String s : split) {
            Answer answer = new Answer();
            count++;
            if (count == num){
                answer.setRight(true);
            }
            answer.setAnswerText(s);
            answer.setQuestion(question);
            answerService.save(answer);
        }

        return "redirect:/admins/home";
    }

    @GetMapping("/all")
    public String allQuestionsByTest(@RequestParam("id") int id, ModelMap modelMap){
        Test byId = testService.getById(id);
        modelMap.addAttribute("test", byId);
        return "adminAllQuestions";

    }


}
