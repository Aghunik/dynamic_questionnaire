package com.example.springmvc.service;

import com.example.springmvc.model.User;
import com.example.springmvc.model.enums.UserType;

import java.util.List;

public interface UserService {

    void save(User user);

    void delete(int id);

    User getByEmail(String email);

    User getById(int id);

    List<User> get();

    List<User> getByUserType(UserType userType);

}
