package com.example.springmvc.service;

import com.example.springmvc.model.Category;
import com.example.springmvc.model.Test;

import java.util.List;

public interface TestService {

    void save(Test test);

    void delete(int id);

    void deletQuestionById(int testId, int id);

    Test getById(int id);

    List<Test> get();

}
