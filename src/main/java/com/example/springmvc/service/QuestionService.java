package com.example.springmvc.service;

import com.example.springmvc.model.Category;
import com.example.springmvc.model.Question;

import java.util.List;

public interface QuestionService {

    void save(Question question);

    void delete(int id);

    Question getById(int id);

    List<Question> getByCategory(Category category);

    List<Question> get();
}
