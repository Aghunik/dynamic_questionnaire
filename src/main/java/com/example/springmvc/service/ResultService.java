package com.example.springmvc.service;

import com.example.springmvc.model.Result;

import java.util.List;

public interface ResultService {

    void save(Result result);

    void delete(int id);

    Result getById(int id);

    List<Result> getByTestId(int id);

    List<Result> getByUserId(int id);

    List<Result> get();

}
