package com.example.springmvc.service;

import com.example.springmvc.model.Answer;

import java.util.List;

public interface AnswerService {

    void save(Answer answer);

    void delete(int id);

    Answer getById(int id);

    List<Answer> getByQuestionId(int id);

    List<Answer> get();

}
