package com.example.springmvc.service;

import com.example.springmvc.model.Category;


import java.util.List;

public interface CategoryService {

    void save(Category category);

    void delete(int id);

    Category getByName(String name);

    Category getById(int id);

    List<Category> get();

}
