package com.example.springmvc.service.impl;

import com.example.springmvc.model.Category;
import com.example.springmvc.model.Test;
import com.example.springmvc.repository.CategoryRepository;
import com.example.springmvc.repository.TestRepository;
import com.example.springmvc.service.CategoryService;
import com.example.springmvc.service.TestService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TestServiceImpl implements TestService {


    private final TestRepository testRepository;


    @Override
    public void save(Test test) {
        testRepository.save(test);
    }

    @Override
    public void delete(int id) {
        testRepository.deleteById(id);

    }

    @Override
    public void deletQuestionById(int testId, int id) {
        testRepository.deleteQuestionById(testId, id);
    }


    @Override
    public Test getById(int id) {
        Optional<Test> byId = testRepository.findById(id);
        if (byId.isPresent()) {
            return byId.get();
        }
        return null;
    }


    @Override
    public List<Test> get() {
        return testRepository.findAll();
    }
}
