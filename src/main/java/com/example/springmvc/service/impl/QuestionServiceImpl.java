package com.example.springmvc.service.impl;


import com.example.springmvc.model.Category;
import com.example.springmvc.model.Question;
import com.example.springmvc.repository.QuestionRepository;
import com.example.springmvc.service.QuestionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;


    @Override
    public void save(Question question) {
        questionRepository.save(question);
    }

    @Override
    public void delete(int id) {
        questionRepository.deleteById(id);
    }

    @Override
    public Question getById(int id) {
        Optional<Question> byId = questionRepository.findById(id);
        if (byId.isPresent()) {

            return byId.get();
        }
        return null;
    }

    @Override
    public List<Question> getByCategory(Category category) {
        return questionRepository.findByCategory(category);
    }

    @Override
    public List<Question> get() {
        return questionRepository.findAll();
    }
}
