package com.example.springmvc.service.impl;

import com.example.springmvc.model.Result;
import com.example.springmvc.repository.ResultRepository;
import com.example.springmvc.service.ResultService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ResultServiceImpl implements ResultService{


    private final ResultRepository resultRepository;


    @Override
    public void save(Result result) {
        resultRepository.save(result);
    }



    @Override
    public void delete(int id) {
        resultRepository.deleteById(id);
    }



    @Override
    public Result getById(int id) {
        Optional<Result> byId = resultRepository.findById(id);
        if (byId.isPresent()){
            return byId.get();
        }
        return null;
    }

    @Override
    public List<Result> getByTestId(int id) {
        return resultRepository.findByTestId(id);
    }

    @Override
    public List<Result> getByUserId(int id) {
        return resultRepository.findByUserId(id);
    }


    @Override
    public List<Result> get() {
        return resultRepository.findAll();
    }
}
