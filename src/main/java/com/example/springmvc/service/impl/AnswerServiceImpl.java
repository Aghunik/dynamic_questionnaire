package com.example.springmvc.service.impl;

import com.example.springmvc.model.Answer;
import com.example.springmvc.model.Category;
import com.example.springmvc.model.Test;
import com.example.springmvc.repository.AnswerRepository;
import com.example.springmvc.repository.TestRepository;
import com.example.springmvc.service.AnswerService;
import com.example.springmvc.service.TestService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AnswerServiceImpl implements AnswerService {


    private final AnswerRepository answerRepository;


    @Override
    public void save(Answer answer) {
        answerRepository.save(answer);
    }

    @Override
    public void delete(int id) {
        answerRepository.deleteById(id);

    }



    @Override
    public Answer getById(int id) {
        Optional<Answer> byId = answerRepository.findById(id);
        if (byId.isPresent()){
            return byId.get();
        }
        return null;
    }

    @Override
    public List<Answer> getByQuestionId(int id) {

        return answerRepository.findByQuestionId(id);
    }

    @Override
    public List<Answer> get() {
        return answerRepository.findAll();
    }
}
