package com.example.springmvc.service.impl;

import com.example.springmvc.model.Category;
import com.example.springmvc.repository.CategoryRepository;
import com.example.springmvc.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CategoryServiceImpl implements CategoryService {


    private final CategoryRepository categoryRepository;


    @Override
    public void save(Category category) {
        categoryRepository.save(category);
    }

    @Override
    public void delete(int id) {


    }

    @Override
    public Category getByName(String name) {
        return categoryRepository.findByName(name);
    }

    @Override
    public Category getById(int id) {
        Optional<Category> byId = categoryRepository.findById(id);
        if (byId.isPresent()) {

            return byId.get();
        }
        return null;
    }

    @Override
    public List<Category> get() {
        return categoryRepository.findAll();
    }
}
