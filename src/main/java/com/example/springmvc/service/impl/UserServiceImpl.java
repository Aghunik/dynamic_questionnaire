package com.example.springmvc.service.impl;

import com.example.springmvc.model.User;
import com.example.springmvc.model.enums.UserType;
import com.example.springmvc.repository.UserRepository;
import com.example.springmvc.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;

     @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void delete(int id) {
        userRepository.deleteById(id);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User getById(int id) {
        Optional<User> byId = userRepository.findById(id);
        if (byId.isPresent()) {

            return byId.get();
        }
        return null;
    }

    @Override
    public List<User> get() {
        return userRepository.findAll();
    }

    @Override
    public List<User> getByUserType(UserType userType) {
        return userRepository.findUsersByUserType(userType);
    }

}
