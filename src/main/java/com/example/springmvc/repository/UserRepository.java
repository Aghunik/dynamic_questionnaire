package com.example.springmvc.repository;

import com.example.springmvc.model.User;
import com.example.springmvc.model.enums.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    User findByEmail(String email);
    List<User> findUsersByUserType(UserType userType);
}
