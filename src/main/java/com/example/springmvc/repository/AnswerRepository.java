package com.example.springmvc.repository;

import com.example.springmvc.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AnswerRepository extends JpaRepository<Answer,Integer> {
   List<Answer> findByQuestionId(int id);
}
