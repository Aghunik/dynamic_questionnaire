package com.example.springmvc.repository;

import com.example.springmvc.model.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Repository
public interface TestRepository extends JpaRepository<Test,Integer> {

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM test_question WHERE test_id = :testId AND question_id = :questionId", nativeQuery = true)
    void deleteQuestionById(@Param("testId") int testId, @Param("questionId") int id);

}
