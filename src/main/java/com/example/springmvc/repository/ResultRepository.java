package com.example.springmvc.repository;

import com.example.springmvc.model.Answer;
import com.example.springmvc.model.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ResultRepository extends JpaRepository<Result,Integer> {
    List<Result> findByTestId(int id);
    List<Result> findByUserId(int id);
}
