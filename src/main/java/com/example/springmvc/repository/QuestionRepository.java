package com.example.springmvc.repository;

import com.example.springmvc.model.Category;
import com.example.springmvc.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface QuestionRepository extends JpaRepository<Question,Integer> {
    List<Question> findByCategory(Category category);

}
